const express = require('express');
const router = express.Router();

const {
    getUserById,
    deleteUserById
} = require('../services/usersService');

const {
    changeUserPassword
} = require('../services/authService');

const { validatePasswordAsync } = require('../utils/validatorUtil');

const {
    asyncWrapper
} = require('../utils/apiUtils');
const {
    InvalidRequestError
} = require('../utils/errors');

router.get('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;

    const user = await getUserById(userId);

    if (!user) {
        throw new InvalidRequestError('No user with such id found!');
    }

    const { _id, username, createdDate } = user;

    res.json({ user: { _id, username, createdDate } });
}));

router.delete('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;

    await deleteUserById(userId);

    res.json({ message: "User deleted successfully" });
}));

router.patch('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { oldPassword, newPassword } = req.body;

    await changeUserPassword(userId, oldPassword, newPassword);

    res.json({ message: "User updated successfully" });
}));

module.exports = {
    usersRouter: router
}