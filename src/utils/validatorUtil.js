const Joi = require('joi');

const validatePasswordAsync = async (password) => {
    const schema = Joi.object({
        password: Joi.string()
            .min(6)
            .max(20)
            .required()
    });

    await schema.validateAsync({ password });
}

const validateUsernameAsync = async (username) => {
    const schema = Joi.object({
        username: Joi.string()
            .alphanum()
            .min(3)
            .max(15)
            .required()
    });

    await schema.validateAsync({ username });
}

module.exports = {
    validatePasswordAsync,
    validateUsernameAsync
}