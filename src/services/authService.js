const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const { User } = require('../models/userModel');
const { InvalidRequestError } = require('../utils/errors');
const { validatePasswordAsync } = require('../utils/validatorUtil')

const cryptPass = async (password) => await bcrypt.hash(password, 10);

const registration = async ({ username, password }) => {
    const cryptedPass = await cryptPass(password);

    const user = new User({
        username,
        password: cryptedPass
    });

    await user.save();
}

const changeUserPassword = async (userId, oldPassword, newPassword) => {
    const user = await User.findOne({ _id: userId });

    if (!user) {
        throw new InvalidRequestError('No user with such id found!');
    }

    try {
        await validatePasswordAsync(newPassword);
    } catch (e) {
        throw new InvalidRequestError(`The new pass didn't match the criteria.`);
    }

    if (!(await bcrypt.compare(oldPassword, user.password))) {
        throw new InvalidRequestError('Wrong old password.');
    }

    user.password = await cryptPass(newPassword);

    await User.findOneAndUpdate({ _id: userId }, { $set: user });
};

const login = async ({ username, password }) => {
    const user = await User.findOne({ username });

    if (!user) {
        throw new InvalidRequestError('Invalid username or password');
    }

    if (!(await bcrypt.compare(password, user.password))) {
        throw new InvalidRequestError('Invalid username or password');
    }

    const token = jwt.sign({
        _id: user._id,
        username: user.username
    }, 'secret');
    return token;
}

module.exports = {
    registration,
    login,
    changeUserPassword
};