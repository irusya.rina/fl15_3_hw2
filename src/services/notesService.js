const nodemon = require('nodemon');
const { Note } = require('../models/noteModel');

const { InvalidRequestError } = require('../utils/errors');

const getNotesByUserId = async (userId, offset, limit) => {
    const notes = await Note.find({ userId }).skip(offset).limit(limit);
    const count = await Note.find({ userId }).count();

    return { notes, count };
}

const getNoteByIdForUser = async (noteId, userId) => {
    const note = await Note.findOne({ _id: noteId, userId });
    return note;
}

const addNoteToUser = async (userId, notePayload) => {
    const note = new Note({ ...notePayload, userId });
    await note.save();
}

const updateNoteByIdForUser = async (noteId, userId, data) => {
    const note = await Note.findOne({ _id: noteId, userId });

    if (!note) {
        throw new InvalidRequestError('No note with such id found!');
    }

    await Note.findOneAndUpdate({ _id: noteId, userId }, { $set: data });
}

const updateNoteCompletedByIdForUser = async (noteId, userId) => {
    const note = await Note.findOne({ _id: noteId, userId });

    if (!note) {
        throw new InvalidRequestError('No note with such id found!');
    }

    await Note.findOneAndUpdate({ _id: noteId, userId }, { $set: { completed: !note.completed } });
}

const deleteNoteByIdForUser = async (noteId, userId) => {
    const note = await getNoteByIdForUser(noteId, userId);

    if (!note) {
        throw new InvalidRequestError('No note with such id found!');
    }

    await Note.findOneAndRemove({ _id: noteId, userId });
}

module.exports = {
    getNotesByUserId,
    getNoteByIdForUser,
    addNoteToUser,
    updateNoteByIdForUser,
    updateNoteCompletedByIdForUser,
    deleteNoteByIdForUser
};