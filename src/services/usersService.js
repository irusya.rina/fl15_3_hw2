const { User } = require('../models/userModel');

const { InvalidRequestError } = require('../utils/errors');

const getUserById = async (userId) => {
    const user = await User.findOne({ _id: userId });
    return user;
}

const deleteUserById = async (userId) => {
    const user = await getUserById(userId);

    if (!user) {
        throw new InvalidRequestError('No user with such id found!');
    }

    await User.findOneAndRemove({ _id: userId });
}

module.exports = {
    getUserById,
    deleteUserById
};