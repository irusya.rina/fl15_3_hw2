const { validatePasswordAsync, validateUsernameAsync } = require('../utils/validatorUtil');

const registrationValidator = async (req, res, next) => {
    try {
        await validateUsernameAsync(req.body.username);
        await validatePasswordAsync(req.body.password);
        next();
    }
    catch (err) {
        next(err);
    }
};

module.exports = {
    registrationValidator,
}