const express = require('express');
const path = require('path');
const morgan = require('morgan')
const mongoose = require('mongoose');
const app = express();

const { usersRouter } = require('./controllers/usersController');
const { notesRouter } = require('./controllers/notesController');
const { authRouter } = require('./controllers/authController');
const { authMiddleware } = require('./middlewares/authMiddleware');
const { NodeCourseError, InvalidRequestError } = require('./utils/errors');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', [authMiddleware], usersRouter);
app.use('/api/notes', [authMiddleware], notesRouter);

app.use((req, res, next) => {
    res.status(404).json({ message: 'Not found' })
});

app.use((err, req, res, next) => {

    if (err instanceof mongoose.Error.CastError
        || err instanceof mongoose.Error.ValidationError
        || err.name === 'MongoError') {

        const invalidRequestErr = new InvalidRequestError();
        return res.status(invalidRequestErr.status).json({ message: invalidRequestErr.message });
    }

    if (err instanceof NodeCourseError) {
        return res.status(err.status).json({ message: err.message });
    }
    res.status(500).json({ message: err.message });
});

const start = async () => {
    try {
        const dbUrl = 'mongodb+srv://test-user:D4UMfqg8pbHUwT3@cluster0.3bkhg.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';

        await mongoose.connect(dbUrl, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });

        app.listen(8080);
    } catch (err) {
        console.error(`Error on server startup: ${err.message}`);
    }
}

start();
